# Installazione

## Server

L'applicazione gira su Tomcat e non ha dipendenze su servizi esterni, è sufficiente copiare il war file dist/mrfaac.war nella cartella di deploy dell'application server.

### Compilazione

Il progetto è compilabile con Java 1.6+ e Maven 3.

## Client Web

Il client è una semplice applicazione standalone HTML/CSS/Knockout JS, basta aprire il file index.html in un browser direttamente da filesystem.

L'url di default è _http://localhost:8080/mrfaac/login_ nel caso si sia installato il server ad un url differente è necessario modificare la variabile LOGIN_SERVICE_URL nel file client/js/LoginViewModel.js

## Client WS

L'URL del WSDL è _http://localhost:8080/mrfaac/services/appMonitor?wsdl_

mentre l'endpoint è _http://localhost:8080/mrfaac/services/appMonitor_

Anche in questo caso la prima parte degli URL potrebbe richiedere adattamenti a seconda dell'installazione.

# Scelte progettuali

L'applicazione server ha un'architettura basata su Spring 4, Hibernate 4 e CXF.

Ho strutturato il progetto immaginando un'applicazione che fornisca servizi REST, motivo per cui il login ritorna un token di autenticazione che può essere usato dai client per richiamare ulteriori servizi senza rieseguire l'autenticazione tramite username e password.

## I token di autenticazione

I token hanno una validità (configurabile) di 1 minuto, è inoltre presente un job Quartz che provvede alla rimozione dei token scaduti ogni 30 secondi (anch'esso configurabile).
Questi valori sono volutamente bassi per favorire il testing, in un ambiente di produzione andrebbero aumentati cercando il giusto compromesso tra sicurezza e performance.

## Java

Per garantire la portabilità il progetto è compilato con java 6 (la versione minima richiesta da Spring 4).

## DB

Trattandosi di un test come database si è optato per HSQLDB per semplificare sia lo  sviluppo che l'installazione.
Il DB parte all'avvio dell'applicazione caricando un paio di utenti tramite lo script _server/src/main/resources/test-data.sql_.

## Spring

Ho utilizzato un approccio misto nella configurazione di Spring:

* XML per tutto ciò che riguarda l'infrastruttura (hibernate, cxf, quartz)

* annotazioni per dao, servizi e controller REST

Dalla mia esperienza ho riscontrato che le annotazioni per questo tipo di componenti permettono di velocizzare i tempi di sviluppo senza compromettere manuntenibilità e comprensione dell'architettura.

Come si può notare dai file di configurazione di Spring l'autodiscovery tramite component-scan è limitato ai soli package preposti a contenere i componenti citati.

I file di configurazione sono così suddivisi:

* server/src/main/resources/applicationContext.xml -> service e dao layer (include applicationContext-dao.xml e applicationContext-service.xml)

* server/src/main/webapp/WEB-INF/applicationContext-web.xml -> web layer (controller REST)

* server/src/main/webapp/WEB-INF/cxf-servlet.xml -> web service

## Web service

Il web service è stato implementato con CXF, framework molto comodo per implementare ws di tipo contract-last. Il WSDL è generato a runtime, in un'applicazione in produzione andrebbe configurato in maniera da fornire un WSDL statico.

## Sicurezza

Alcune brevi considerazioni sulla sicurezza:

* su DB le password sono salvate con codifica MD5

* in un ambiente di produzione le comunicazioni dovrebbero avvenire in HTTPS dato che tra client e server la password viaggia in chiaro

* in un'applicazione reale si dovrebbe probabilmente introdurre lato server un sistema di autenticazione e autorizzazioni più sofisticato (come Spring Security) che protegga anche il WS

## 2 parole sul client Web

Ho deciso di implementarlo con Knockout JS, che non conoscevo, per iniziare a saggiarlo e farmi un'idea. È abbastanza rudimentale e contiene sicuramente qualche ingenuità.
Per la UI ho usato l'ormai onnipresente Bootstrap.
