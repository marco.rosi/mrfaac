var LOGIN_SERVICE_URL = 'http://localhost:8080/mrfaac/login';

var LoginViewModel = function() {

	var self = this;

	self.username = ko.observable("");
	self.password = ko.observable("");
	self.token = ko.observable("");
	self.anonymous = ko.observable(true);
	self.logged = ko.observable(false);

	self.login = function() {

		console.log("logging in");

		$.ajax({
				type: "POST",
				crossDomain: true,
				url: LOGIN_SERVICE_URL,
				data: {username:self.username, password:self.password},
				success : function(response){
					console.log(response);
					self.logged(true);
					self.anonymous(false);
					self.token(response.token);
					$("#error_msg").hide();
				},
				error : function(err) {
					console.log(err);
					$("#error_msg").fadeIn();
				}
		});

	}
}

ko.applyBindings(new LoginViewModel());
