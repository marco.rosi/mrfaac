package it.faac.assessment.dao;

import com.google.common.base.Preconditions;
import it.faac.assessment.model.LoggedUser;
import it.faac.assessment.model.User;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * This is the DAO for {@link it.faac.assessment.model.User} and related classes
 */
@Repository
public class AuthenticationDao {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * @return the {@link User} with the passed username parameter, null if not found
	 */
	public User find(String username) {

		Preconditions.checkArgument(StringUtils.isNotBlank(username));

		try {
			return (User) entityManager
					.createQuery("from User where username=:username")
					.setParameter("username",username).getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public void save(LoggedUser loggedUser) {
		Preconditions.checkNotNull(loggedUser);
		entityManager.persist(loggedUser);
	}

	public List<LoggedUser> findAllLoggedUsers() {
		return entityManager.createQuery("from LoggedUser ").getResultList();
	}

	public void delete(LoggedUser user) {
		Preconditions.checkNotNull(user);
		entityManager.remove(user);
	}
}
