package it.faac.assessment.frontend;

import it.faac.assessment.service.AuthenticationService;
import it.faac.assessment.service.BadAuthenticationException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * This is the REST controller for user login.
 *
 * It respond at URL '/login' and expects 'username' and 'password' as request parameter.
 *
 * If authentication is successfull returns a json representation of {@link it.faac.assessment.model.AuthenticatedUser}
 * otherwise returns an HTTP 401 (unauthorized) response
 *
 */
@RestController
public class LoginRestController {

	@Autowired
	AuthenticationService authenticationService;

	@RequestMapping("/login")
	public Object login(@RequestParam(value="username") String username,
			@RequestParam(value="password") String password)
	{
		try {
			if(StringUtils.isBlank(username) || StringUtils.isBlank(password))
				throw new BadAuthenticationException();

			return authenticationService.login(username,password);

		} catch (BadAuthenticationException e) {
			//this will result in a HTTP 401
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		}
	}
}
