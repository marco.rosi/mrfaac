package it.faac.assessment.model;

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * This class contains the information of a successful authentication ready to be sent to a client.
 * <br />
 * It contains the username of the authenticated user and a <strong>token</strong> what can be
 * used by the client to perform subsequent communication with the service without reauthenticate.
 */
public class AuthenticatedUser implements Serializable {

	private String username;
	private String token;

	public AuthenticatedUser(User user, String token) {

		Preconditions.checkNotNull(user);
		Preconditions.checkArgument(StringUtils.isNoneBlank(token));

		this.username=user.getUsername();
		this.token=token;
	}

	protected AuthenticatedUser(){};

	public String getUsername() {
		return username;
	}

	public String getToken() {
		return token;
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof AuthenticatedUser))
			return false;

		AuthenticatedUser that = (AuthenticatedUser) o;

		if (!username.equals(that.username))
			return false;
		return token.equals(that.token);

	}

	@Override public int hashCode() {
		int result = username.hashCode();
		result = 31 * result + token.hashCode();
		return result;
	}

	@Override public String toString() {
		final StringBuilder sb = new StringBuilder("AuthenticatedUser{");
		sb.append("username='").append(username).append('\'');
		sb.append(", token='").append(token).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
