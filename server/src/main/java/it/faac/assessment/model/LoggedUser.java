package it.faac.assessment.model;

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Date;

/**
 * This entity represent a user logged into the system, it's persisted on DB
 */
@Entity
public class LoggedUser implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	private User user;

	private String token;

	private Date loginDate;

	private Date expirationDate;

	public LoggedUser(User user, String token, Date expirationDate) {

		Preconditions.checkNotNull(user);
		Preconditions.checkArgument(StringUtils.isNoneBlank(token));
		Preconditions.checkNotNull(expirationDate);

		this.user=user;
		this.token=token;
		this.loginDate=new Date();
		this.expirationDate=expirationDate;
	}

	/** default constructor per hibernate */
	protected LoggedUser(){};

	/**
	 * @return true if the authentication token of this logged user is expired
	 */
	public boolean isExpired() {
		return new Date().after(expirationDate);
	}

	public Long getId() {
		return id;
	}

	public User getUser() {
		return user;
	}

	public String getToken() {
		return token;
	}

	public Date getLoginDate() {
		return loginDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	@Override public String toString() {
		final StringBuilder sb = new StringBuilder("LoggedUser{");
		sb.append("id=").append(id);
		sb.append(", user=").append(user);
		sb.append(", token='").append(token).append('\'');
		sb.append(", loginDate=").append(loginDate);
		sb.append(", expirationDate=").append(expirationDate);
		sb.append('}');
		return sb.toString();
	}
}
