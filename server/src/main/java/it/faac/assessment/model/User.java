package it.faac.assessment.model;

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * This is the User entity, it's persisted on DB
 */
@Entity
public class User implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String username;

	private String password;

	public User(String username, String password) {

		Preconditions.checkArgument(StringUtils.isNoneBlank(username));
		Preconditions.checkArgument(StringUtils.isNoneBlank(password));

		this.username=username;
		this.password=password;
	}

	/** default constructor per hibernate */
	protected User() {};

	public Long getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	/**
	 * @return true if the password of this user matches the password passed as parameter
	 */
	public boolean hasPassword(String passwordToCheck) {
		return password.equals(passwordToCheck);
	}

	@Override public String toString() {
		final StringBuilder sb = new StringBuilder("User{");
		sb.append("id=").append(id);
		sb.append(", username='").append(username).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
