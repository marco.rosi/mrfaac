package it.faac.assessment.service;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import it.faac.assessment.dao.AuthenticationDao;
import it.faac.assessment.model.AuthenticatedUser;
import it.faac.assessment.model.LoggedUser;
import it.faac.assessment.model.User;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * This service is responsible for user authentication and managing of logged users.
 * <br />
 * After a successfull login the user will be registered in the system with a unique token assigned.
 * <br />
 * The tokens grant access to the system for a {@link #tokenExpirationTimeInMinutes} minutes (this value can be configured
 * with dependency injection), use the method {@link #checkToken(String)} to verify token validity.
 * <br />
 * Please note what <strong>a user can have multiple active authentication</strong> at the same time, each
 * one will be registered with a different token.
 */
@Service
public class AuthenticationService {

	@Autowired
	private AuthenticationDao authenticationDao;

	@Autowired
	private Integer tokenExpirationTimeInMinutes;

	/**
	 * This method is responsible for user authentication, if username and password are
	 * registered in the system the method will return an {@link AuthenticatedUser}
	 * <br />
	 * If the user is not present or the password doesn't match a BadAuthenticationException
	 * will be throw.
	 *
	 * @param username the username of the user
	 * @param password the user password
	 *
	 * @return an instance of {@link AuthenticatedUser}
	 */
	@Transactional(readOnly = false)
	public AuthenticatedUser login(String username, String password) throws BadAuthenticationException{

		Preconditions.checkArgument(StringUtils.isNotBlank(username));
		Preconditions.checkArgument(StringUtils.isNotBlank(password));

		User user = authenticationDao.find(username);

		if(user==null)
			throw new BadAuthenticationException(String.format("Bad authentication for %s",username));

		//we store password in md5 hex format
		String md5Password = md5(password);

		if(user.hasPassword(md5Password)){
			return authenticate(user);
		} else {
			throw new BadAuthenticationException(String.format("Bad authentication for %s",username));
		}
	}

	/**
	 * verify that a token is registered in the system and not expired
	 *
	 * @param token a token obtained after a {@link #login(String, String)}
	 * @throws InvalidTokenException
	 */
	public void checkToken(String token) throws InvalidTokenException{

		if(StringUtils.isBlank(token))
			throw new InvalidTokenException();

		for (LoggedUser loggedUser : getLoggedUsers()) {
			if(loggedUser.getToken().equals(token))
				return;
		}

		throw new InvalidTokenException();
	}

	/**
	 * @return the MD5 hash in hex format for the password passed as parameter
	 */
	private String md5(String password) {

		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(password.getBytes());
			byte[] bytes = md.digest();
			StringBuilder sb = new StringBuilder();
			for(int i=0; i< bytes.length ;i++)
			{
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			return sb.toString();

		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Create an AuthenticathedUser with a unique random token what will be valid
	 * for {@see tokenExpirationTimeInMinutes} minutes, finally register the new logged user on DB
	 *
	 * @param user the user to authenticate
	 *
	 * @return an instance of {@link AuthenticatedUser}
	 */
	private AuthenticatedUser authenticate(User user) {

		String token = UUID.randomUUID().toString();

		Date expirationDate = new Date(System.currentTimeMillis()
				+ TimeUnit.MINUTES.toMillis(tokenExpirationTimeInMinutes));

		LoggedUser logged = new LoggedUser(user, token, expirationDate);

		authenticationDao.save(logged);

		return new AuthenticatedUser(user,token);
	}

	/**
	 * @return a collection of {@link LoggedUser}, each one representing a user currently
	 * logged into the system, user with an expired token will NOT be included
	 */
	public Collection<LoggedUser> getLoggedUsers() {

		List<LoggedUser> allLoggedUsers = authenticationDao.findAllLoggedUsers();

		//returns only not expired users
		return Collections2.filter(allLoggedUsers, new Predicate<LoggedUser>() {
			public boolean apply(LoggedUser loggedUser) {
				return !loggedUser.isExpired();
			}
		});
	}

	/**
	 * removes any {@link LoggedUser} with an expired token, useful to be invoked by a scheduled job
	 */
	@Transactional(readOnly = false)
	public void cleanExpiredToken() {

		List<LoggedUser> allLoggedUsers = authenticationDao.findAllLoggedUsers();
		for (LoggedUser user : allLoggedUsers) {
			if(user.isExpired())
				authenticationDao.delete(user);
		}
	}

}
