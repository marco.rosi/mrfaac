package it.faac.assessment.service;

/**
 * Represent a bad authentication attempt
 */
public class BadAuthenticationException extends Throwable {
	public BadAuthenticationException() {
		super();
	}
	public BadAuthenticationException(String msg) {
		super(msg);
	}
}
