package it.faac.assessment.service;

/**
 * Throwed when a user try to use an invalid token
 */
public class InvalidTokenException extends Throwable {
	public InvalidTokenException() {
		super();
	}
	public InvalidTokenException(String msg) {
		super(msg);
	}
}
