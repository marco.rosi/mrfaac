package it.faac.assessment.ws;

import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;
import java.util.Collection;

/**
 * This is the web service interface exposed for application monitoring
 */
@WebService
public interface AppMonitorWS {

	/**
	 * @return the users with a valid authentication token currently logged in the system
	 */
	@XmlElement(name = "user") Collection<LoggedUserDto> getLoggedUsers();
}
