package it.faac.assessment.ws;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;

import it.faac.assessment.model.LoggedUser;
import it.faac.assessment.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jws.WebService;
import java.util.Collection;

/**
 * The implementation of {@link AppMonitorWS}
 */
@WebService(endpointInterface = "it.faac.assessment.ws.AppMonitorWS")
public class AppMonitorWSImpl implements AppMonitorWS {

	@Autowired
	private AuthenticationService authenticationService;

	public Collection<LoggedUserDto> getLoggedUsers() {

		Collection<LoggedUser> loggedUsers = authenticationService.getLoggedUsers();

		Collection<LoggedUserDto> result = Collections2
				.transform(loggedUsers, new Function<LoggedUser, LoggedUserDto>() {
					public LoggedUserDto apply(LoggedUser loggedUser) {
						return new LoggedUserDto(loggedUser);
					}
				});

		return result;
	}

	public void setAuthenticationService(AuthenticationService authenticationService) {
		this.authenticationService = authenticationService;
	}
}
