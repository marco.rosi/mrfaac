package it.faac.assessment.ws;

import com.google.common.base.Preconditions;
import it.faac.assessment.model.LoggedUser;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import java.util.Date;

/**
 * A DTO to pass user information via WS
 */
@XmlRootElement(name = "loggedUser")
public class LoggedUserDto {

	@XmlAttribute
	private String username;
	@XmlAttribute
	private String token;
	@XmlAttribute
	private Date loginDate;
	@XmlAttribute
	private Date expireDate;

	public LoggedUserDto(LoggedUser loggedUser) {
		Preconditions.checkNotNull(loggedUser);
		username=loggedUser.getUser().getUsername();
		token=loggedUser.getToken();
		loginDate=loggedUser.getLoginDate();
		expireDate=loggedUser.getExpirationDate();
	}

	protected LoggedUserDto() {};

	public String getUsername() {
		return username;
	}

	public String getToken() {
		return token;
	}

	public Date getLoginDate() {
		return loginDate;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	@Override public String toString() {
		final StringBuilder sb = new StringBuilder("LoggedUserDto{");
		sb.append("username='").append(username).append('\'');
		sb.append(", token='").append(token).append('\'');
		sb.append(", loginDate=").append(loginDate);
		sb.append(", expireDate=").append(expireDate);
		sb.append('}');
		return sb.toString();
	}
}
