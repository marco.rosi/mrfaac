package it.faac.assessment.dao;

import it.faac.assessment.model.LoggedUser;
import it.faac.assessment.model.User;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * This is the test for {@link AuthenticationDao}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext-dao.xml")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@Transactional
public class AuthenticationDaoTest {

	@Autowired
	private AuthenticationDao authenticationDao;

	@Test
	public void testFind() {

		assertNull(authenticationDao.find("nonesisto"));

		User john = authenticationDao.find("john");
		assertNotNull(john);
		assertEquals("john",john.getUsername());
	}

	@Test
	public void testSaveLoggedUser() {

		User john = authenticationDao.find("john");

		String token = "aaa";
		Date expirationDate = new Date();

		LoggedUser logged = new LoggedUser(john,token,expirationDate);

		authenticationDao.save(logged);

		List<LoggedUser> all = authenticationDao.findAllLoggedUsers();
		assertTrue(all.contains(logged));
		assertEquals(john,all.get(0).getUser());
	}
}
