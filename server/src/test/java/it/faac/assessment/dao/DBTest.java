package it.faac.assessment.dao;

import it.faac.assessment.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * This test check the configuration of JPA and the DB
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext-dao.xml")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@Transactional
public class DBTest {

	@Autowired
	private EntityManagerFactory entityManagerFactory;

	private EntityManager entityManager;

	@Before
	public void init() {
		entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
	}

	@Test
	public void checkConfig() {
		assertNotNull(entityManager);
	}

	@Test
	public void dbInitialized() {
		List<User> results = entityManager.createQuery("from User ").getResultList();
		assertEquals(2,results.size());
	}

	@Test
	public void persistUser() {

		User user = new User("luther","blisset");

		entityManager.persist(user);

		assertEquals(1, entityManager.createQuery("from User where username='luther'").getResultList().size());
	}
}
