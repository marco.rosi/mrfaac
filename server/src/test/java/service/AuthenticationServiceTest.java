package service;

import it.faac.assessment.model.AuthenticatedUser;
import it.faac.assessment.model.LoggedUser;
import it.faac.assessment.service.AuthenticationService;

import static org.junit.Assert.*;

import it.faac.assessment.service.BadAuthenticationException;

import it.faac.assessment.service.InvalidTokenException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collection;

/**
 * This is the test for {@link AuthenticationService}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext-service.xml","/applicationContext-dao.xml"})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class AuthenticationServiceTest {

	@Autowired
	private AuthenticationService authenticationService;

	@Test
	public void testBadLogin() throws Throwable {

		try {
			authenticationService.login("john","badpassword");
			fail("login was successful with a wrong password");
		} catch (BadAuthenticationException e) {};
	}

	@Test
	public void testLogin() throws Throwable {

		AuthenticatedUser authUser = authenticationService.login("john", "silver");

		assertNotNull(authUser);
		assertEquals("john", authUser.getUsername());
		assertNotNull(authUser.getToken());

		authenticationService.checkToken(authUser.getToken());
	}

	@Test
	public void testInvalidToken() throws Throwable {
		try {
			authenticationService.checkToken("an_invalid_token");
			fail("the service validated an invalid token");
		} catch (InvalidTokenException e) {};
	}

	@Test
	public void testLoggedUsers() throws Throwable {

		Collection<LoggedUser> users = authenticationService.getLoggedUsers();
		assertNotNull(users);

		int numberOfLoggedUsers = users.size();
		AuthenticatedUser authUser = authenticationService.login("john", "silver");
		assertNotNull(authUser);

		users = authenticationService.getLoggedUsers();
		assertEquals(numberOfLoggedUsers+1,users.size());
		boolean found = false;
		for (LoggedUser user : users) {
			if(user.getToken().equals(authUser.getToken()))
				found = true;
		}
		assertTrue(found);
	}
}
